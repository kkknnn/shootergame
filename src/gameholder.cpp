#include "../include/gameholder.h"
#include <QObject>
#include <QTimer>



GameHolder::GameHolder(QWidget *parent)
{
    WindowResX=800;
    WindowResY=600;
    scene=new QGraphicsScene;
    player=new myPlayer;
    player->setRect(0,0,100,100);
    player->setFlag(QGraphicsItem::ItemIsFocusable);
    player->setFocus();
    scene->addItem(player);
    scene->setSceneRect(0,0,WindowResX,WindowResY);
    setScene(scene);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setFixedSize(WindowResX,WindowResY);
    player->setPos(width()/2,height()-player->rect().height());
    QTimer * EnemyTimer =new QTimer;
    QObject::connect(EnemyTimer,SIGNAL(timeout()),player,SLOT(spawn()));
    EnemyTimer->start(2000);
    show();

}
