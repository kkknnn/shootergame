#include "../include/player.h"
#include <QKeyEvent>
#include <QGraphicsScene>
#include <QObject>
#include "../include/bullet.h"
#include "../include/enemy.h"





void myPlayer::keyPressEvent(QKeyEvent *event)
{

    if(event->key()==Qt::Key_Left)
    {
        if (pos().x()>0)
        {
        setPos(x()-10,y());
        }
    }
    else if(event->key()==Qt::Key_Right)
    {
       if (pos().x()+rect().width()<800)
       {
        setPos(x()+10,y());
       }
    }
    else if(event->key()==Qt::Key_Up)
    {
        if(pos().y()>0)
        {
        setPos(x(),y()-10);
        }
    }
    else if(event->key()==Qt::Key_Down)
    {
        if (pos().y()+rect().height()<600)
        {
        setPos(x(),y()+10);
        }
    }
    else if(event->key()==Qt::Key_Space)
    {
        bullet * _bullet = new bullet();
        _bullet->setPos(x(),y());
        scene()->addItem(_bullet);
    }

}

void myPlayer::spawn()
{
    enemy * _enemy = new enemy();
    scene()->addItem(_enemy);


}
