#include "../include/bullet.h"
#include "../include/enemy.h"
#include <QTimer>
#include <QObject>
#include <QGraphicsScene>
#include <QList>



bullet::bullet()
{
    setRect(0,0,10,50);

    QTimer * timer = new QTimer;
    connect(timer,SIGNAL(timeout()),this,SLOT(move()));

    timer->start(50);

}

void bullet::move()
{

    QList<QGraphicsItem*> ListOfObjects = collidingItems();

    for(auto iterator = ListOfObjects.begin();iterator!=ListOfObjects.end();iterator++)
    {
        if(typeid (*(*iterator))==typeid (enemy))
        {
            scene()->removeItem(*iterator);
            scene()->removeItem(this);
            delete *iterator;
            delete this;
            return;

        }

    }

    setPos(x(),y()-4);
    if (pos().y()+rect().height()<0)
    {
    scene()->removeItem(this);
    delete this;
    }
}
